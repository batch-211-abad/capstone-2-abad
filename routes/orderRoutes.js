const express = require("express");
const router = express.Router();

const orderController = require("../controllers/orderControllers");
const auth = require("../auth");


// Route for creating Order
router.post("/", auth.verify, (req,res)=>{
	
	const data = {

		order: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	orderController.createOrder(data).then(resultFromController=>res.send(resultFromController));
});






module.exports = router;



/*


*/






/*


*/