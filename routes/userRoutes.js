const express = require("express");
const router = express.Router();

const userController = require("../controllers/userControllers");
const auth = require("../auth");

router.post("/checkEmail",(req,res)=>{
	userController.checkEmailExists(req.body).then(resultFromController=>res.send(resultFromController));
})


//Route for user registration
router.post("/register",(req,res)=>{
	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController));
});


//Route for user authentication
router.post("/login",(req,res)=>{
	userController.loginUser(req.body).then(resultFromController=>res.send(resultFromController));
});


//Route for updating Admin Status
router.post("/updateAdmin/:_id", auth.verify, (req,res)=>{
	userController.updateAdmin(req.params, req.body).then(resultFromController=>res.send(resultFromController));
});


//Route for Retrieving User Details
router.get("/getUserDetails", auth.verify, (req,res)=>{

	const userData = auth.decode(req.headers.authorization);

	userController.retrieveUser({userId : userData.id}).then(resultFromController=>res.send(resultFromController));
});

/*

router.get("/details",auth.verify,(req,res)=>{

	// Uses the "decode" method defined in the "auth.js" file to retrieve the user information from the token passing the "token" from the request header as an argument
	const userData = auth.decode(req.headers.authorization);

	// Provides the user's ID for the getProfile controller method
	userController.getProfile({userId : userData.id}).then(resultFromController=>res.send(resultFromController));
});

*/










module.exports = router;