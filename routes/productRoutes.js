const express = require("express");
const router = express.Router();

const productController = require("../controllers/productControllers");
const auth = require("../auth");

// Route for Creating a Product with Admin only privileges.

router.post("/",auth.verify,(req,res)=>{
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
		}
		
	//data.course.name
	productController.createProduct(data).then(resultFromController=>res.send(resultFromController))
});

// Route for Retrieving All Products

router.get("/all", (req,res) => {
	productController.getProducts().then(resultFromController=>res.send(resultFromController));
})

// Route for Retrieving All ACTIVE Products

router.get("/active", (req, res) => {
	productController.getActive().then(resultFromController=>res.send(resultFromController));
});


// Route for Retrieving a SINGLE PRODUCT
router.get("/:productId", (req, res) => {
	productController.getProduct(req.params).then(resultFromController=>res.send(resultFromController));
});



// Route for UPDATING a Product Info

router.put("/:productId", auth.verify, (req,res) =>{
	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
});


//Route for ARCHIVING a Product
router.put("/:productId/archive", auth.verify, (req,res) =>{
	productController.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
});

//Route for ACTIVATING a Product
router.put("/:productId/activate", auth.verify, (req,res) =>{
	productController.activateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
});









module.exports = router;