const Product = require("../models/Product");

// Create a product but Admin only priviliges. 

module.exports.createProduct = (data) => {

	if (data.isAdmin) {
		
		let newProduct = new Product({
		name : data.product.name,
		description : data.product.description,
		price : data.product.price
	});
		return newProduct.save().then((product, error) => {
		if (error) {
			return false;
		} else {

			return newProduct;
		};

	});
}
	else {
		return data;
	}
};


//Retrieve All Products

module.exports.getProducts = () =>{
	return Product.find({}).then(result=>{
		return result;
	});
};


//Retrieve all Active Products

module.exports.getActive = () =>{
	return Product.find({isActive : true}).then(result=>{
		return result;
	});
};


// Retrieve a SINGLE Product

module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result=>{
		return result;
});
};

// Update a Product Info

module.exports.updateProduct = (reqParams, reqBody) => {
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
		if(error) {
			return false;
		}
		else {
			return true;
		};
	});
};

// ARCHIVING a product

module.exports.archiveProduct = (reqParams, reqBody) => {
	let archivedProduct = {
		isActive: false
	};

	return Product.findByIdAndUpdate(reqParams.productId, archivedProduct).then((product, error) => {
		if(error) {
			return false;
		}
		else {
			return false;
		};
	});
};


// ACTIVATING a product

module.exports.activateProduct = (reqParams, reqBody) => {
	let activatedProduct = {
		isActive: true
	};

	return Product.findByIdAndUpdate(reqParams.productId, activatedProduct).then((product, error) => {
		if(error) {
			return false;
		}
		else {
			return true;
		};
	});
};