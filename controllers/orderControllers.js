const Order = require("../models/Order");
const Product = require("../models/Product")
const User = require("../models/User")


// Create Order

/*
	1. Lock the Product ID in a variable
	2. Lock the Quantity in another variable


*/


module.exports.createOrder = (data) => {

	let newOrder = new Order({

		productId : data.order.productId,
		quantity : data.order.quantity
						
	});

	return newOrder.save().then((order, error) => {
		if (error) {
			return false;
		}

		else {
			console.log(error)
			return true;
		};
	});
};



/*



*/


