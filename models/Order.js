/*

Order
userId - string
totalAmount - number,
purchasedOn - date
		     default: new Date(),
products - [

	{
		productId - string,
		quantity - number
	}

]

*/

const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	productId : {
		type: String,
	},
	quantity : {
		type: Number,
		default: 1
	}

});

module.exports = mongoose.model("Order", orderSchema);