/*

Product
name - string,
description - string,
price - number
isActive - boolean
		   default: true,
createdOn - date
			default: new Date()
orders - [
	{
		orderId - string,
		quantity - number

	}
]



*/

const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name : {
		type: String,
		required: [true, "Product Name is Required"]
	},
	description : {
		type: String,
		required: [true, "Product Description is Required"]
	},
	price : {
		type: Number,
		required: [true, "Price is Required"]
	},
	isActive : {
		type: Boolean,
		default: true
	},
	createdOn : {
		type: Date,
		default: new Date ()
	},
	orders : [
		{
			orderId: {
				type: String,
				required: [true, "Order ID is required"]
			},

			quantity: {
				type: Number,
				required: [true, "Quantity is required"]
			}
			
	}]

});

module.exports = mongoose.model("Product", productSchema);